import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  imageOpen: string = "assets/image/on.png"
  imageClose: string = "assets/image/off.png"
  change: boolean = true;

  text: string = ''

  constructor() { }

  ngOnInit(): void {
  }

  onChange(): void {
    this.change = !this.change;
  }

}
